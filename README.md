[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/dwyl/esta/issues)

Hello folks!

**This repo is dedicated for all those code snippets which have never failed to impress and inspire me!**

If they aren't impressing you, kindly feel free to blame me...

Its JavaScript and only JavaScript as of now...

**All the code snippets are self explanatory. However, I have made an attempt to gift you a glimpse of the snippets in their respective files in the form of comments**

*Happy learning!!!*
