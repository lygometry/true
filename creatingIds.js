/*
   No more Libraries
   No more dependencies
   Here is a very simple way of creating ids for your app!

   It intrigued me a lot as it utilises IIFE and Closure to its best
*/

const createId = (function IIFE() {
  let recentId = 0
  return () => {
    let id = Date.now()
    return (recentId = id > recentId ? id : ++recentId)
  }
})()

let createdId = createId()
console.log(createdId)
